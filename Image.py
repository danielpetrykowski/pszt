import cv2
import numpy as np

alfa = 0.6


class Image:
    def __init__(self, path):
        self.img = cv2.imread(path)
        self.sizeXy = [self.img.shape[1], self.img.shape[0]]
        self.overlay = self.img.copy()
        self.output = self.img.copy()
        self.test = np.zeros((self.sizeXy[1],self.sizeXy[0],3), np.uint8)

    def size(self):
        return self.sizeXy

    def draw(self, population):
        self.overlay = self.img.copy()
        self.output = self.img.copy()
        self.test = np.zeros((self.sizeXy[1], self.sizeXy[0], 3), np.uint8)
        for p in population:
            cv2.rectangle(self.overlay, (p.pos[0], p.pos[1]), (p.pos[0] + p.xy[0], p.pos[1] + p.xy[1]),
                          (p.rgb[0], p.rgb[1], p.rgb[2]), -1)
            cv2.rectangle(self.test, (p.pos[0], p.pos[1]), (p.pos[0] + p.xy[0], p.pos[1] + p.xy[1]),
                          (p.rgb[0], p.rgb[1], p.rgb[2]), -1)

    def show(self):
        cv2.addWeighted(self.overlay, alfa, self.output, 1 - alfa,
                        0, self.output)
        cv2.imshow("Output", self.output)
        cv2.waitKey(0)

    def showOv(self):
        cv2.imshow('test', self.test)