import math

maxX = 20
maxY = 30
minX = 5
minY = 5


class Ractangle:
    def __init__(self, imgSize):
        self.minX = 5
        self.minY = 10
        self.maxX = 20
        self.maxY = 30
        self.pos = []
        self.posSigma = []
        self.xy = []
        self.xySigma = []
        self.rgb = []
        self.rgbSigma = []
        self.adapt = -1
        self.imgSize = imgSize

    def conntDisSigma(self, a, b):
        var = ((b + a) ** 2) / 12  # continous ditribution variance
        return math.sqrt(var)  # standard deviation continous distribution

    def calcAdapt(self, img):
        self.adapt = 0
        img = img.img
        for x in range(self.xy[0]):
            for y in range(self.xy[1]):
                sr = math.pow((img[self.pos[1] + y][self.pos[0] + x][0] - self.rgb[0]), 2)
                sg = math.pow((img[self.pos[1] + y][self.pos[0] + x][1] - self.rgb[1]), 2)
                sb = math.pow((img[self.pos[1] + y][self.pos[0] + x][2] - self.rgb[2]), 2)
                self.adapt = self.adapt + sr + sg + sb

    def limitsXy(self):
        if self.xy[0] < minX:
            self.xy[0] = minX
        elif self.xy[0] > maxX:
            self.xy[0] = maxX
        if self.xy[1] < minY:
            self.xy[1] = minY
        elif self.xy[1] > maxY:
            self.xy[1] = maxY

    def limitsPos(self, xy):
        if self.pos[0] < 0:
            self.pos[0] = 0
        elif self.pos[0] > self.imgSize[0] - xy[0]:
            self.pos[0] = self.imgSize[0] - xy[0]
        if self.pos[1] < 0:
            self.pos[1] = 0
        elif self.pos[1] > self.imgSize[1] - xy[1]:
            self.pos[1] = self.imgSize[1] - xy[1]

    def limitsRgb(self):
        for i in range(3):
            if self.rgb[i] < 0:
                self.rgb[i] = 0
            elif self.rgb[i] > 255:
                self.rgb[i] = 255
