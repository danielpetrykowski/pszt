import Image as im
import Evolution as ev

# parameters
mi = 500  # number of generated
lamb = 100
path = 'szach.jpg'
cruci = [1, 1, 1]   # 0 - brak krzyzowania, 1 - uśrednianie, 2 - interpolacja
mutat = [0, 0, 1]   # 0 - brak mutacji 1 - z mutacja


img = im.Image(path)
evo = ev.Evolution(mi, lamb, cruci, mutat, img)
evo.initPopulation()
img.draw(evo.populationP)
img.show()

for i in range(1000):
    evo.creatPopulationT()
    evo.crucifixion()
    evo.mutation()
    evo.selection()
    img.draw(evo.populationP)
    if i % 50 == 0:
        img.showOv()
        img.show()





