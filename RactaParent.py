import random
import Ractangle as ra


class RactaParent(ra.Ractangle):
    def __init__(self, imgSize):
        ra.Ractangle.__init__(self, imgSize)
        self.pos = [random.randint(0, imgSize[0] - self.minX), random.randint(0, imgSize[1] - self.minY)]
        self.posSigma = [self.conntDisSigma(0, imgSize[0] - self.minX), self.conntDisSigma(0, imgSize[1] - self.minY)]
        if imgSize[0] - self.pos[0] < self.maxX:
            x = random.randint(self.minX, imgSize[0] - self.pos[0])
            xSigma = self.conntDisSigma(self.minX, imgSize[0] - self.pos[0])
        else:
            x = random.randint(self.minX, self.maxX)
            xSigma = self.conntDisSigma(self.minX, self.maxX)

        if imgSize[1] - self.pos[1] < self.maxY:
            y = random.randint(self.minY, imgSize[1] - self.pos[1])
            ySigma = self.conntDisSigma(self.minY, imgSize[1] - self.pos[1])
        else:
            y = random.randint(self.minY, self.maxY)
            ySigma = self.conntDisSigma(self.minY, self.maxY)
        self.xy = [x, y]
        self.xySigma = [xSigma, ySigma]
        sigmaRgb = self.conntDisSigma(0, 255)
        r = random.randint(0, 255)
        g = random.randint(0, 255)
        b = random.randint(0, 255)
        self.rgb = [r, g, b]
        self.rgbSigma = [sigmaRgb, sigmaRgb, sigmaRgb]
