import RactaParent as rp
import random
import RactaKid as ra


class Evolution:
    def __init__(self, mi, lamb, cruci, mutat, img):
        self.populationP = []
        self.populationT = []
        self.populationO = []
        self.mi = mi
        self.lamb = lamb
        self.cruci = cruci
        self.mutat = mutat
        self.img = img

    def initPopulation(self):
        for i in range(self.mi):
            rac = rp.RactaParent(self.img.size())
            self.populationP.append(rac)
        self.calcAdapt()

    def creatPopulationT(self):
        self.populationT.clear()
        for i in range(self.lamb):
            x = random.choice(self.populationP)
            self.populationT.append(x)

    def crucifixion(self):
        self.populationO.clear()
        for o in range(0, self.lamb, 2):
            x1 = self.populationT[o]
            x2 = self.populationT[o + 1]
            x = ra.RactaKid(x1, x2, self.cruci, self.img.size())
            self.populationO.append(x)

    def mutation(self):
        for o in self.populationO:
            o.mutation(self.mutat)

    def selection(self):
        self.calcAdaptNew()
        populationPO = self.populationP + self.populationO
        populationPO.sort(key=lambda x: x.adapt)
        self.populationP = populationPO[:self.mi]

    def calcAdapt(self):
        for p in self.populationP:
            p.calcAdapt(self.img)

    def calcAdaptNew(self):
        for p in self.populationO:
            p.calcAdapt(self.img)