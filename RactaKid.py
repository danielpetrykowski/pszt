import Ractangle as ra
import numpy as np
import random
import math


class RactaKid(ra.Ractangle):
    def __init__(self, p1, p2, cruci, imgSize):
        ra.Ractangle.__init__(self, imgSize)

        if cruci[1] == 0:
            self.xy = p1.xy
            self.xySigma = p1.xySigma
        elif cruci[1] == 1:
            xyP1 = np.array(p1.xy)
            xyP2 = np.array(p2.xy)
            xyP1Sigma = np.array(p1.xySigma)
            xyP2Sigma = np.array(p2.xySigma)
            self.xy = ((xyP1 + xyP2) / 2).astype(int).tolist()
            self.xySigma = ((xyP1Sigma + xyP2Sigma) / 2).tolist()
        elif cruci[1] == 2:
            a = random.random()
            xyP1 = np.array(p1.xy)
            xyP2 = np.array(p2.xy)
            xyP1Sigma = np.array(p1.xySigma)
            xyP2Sigma = np.array(p2.xySigma)
            self.xy = (a * xyP1 + (1 - a) * xyP2).astype(int).tolist()
            self.xySigma = (a * xyP1Sigma + (1 - a) * xyP2Sigma).tolist()
        self.limitsXy()

        if cruci[0] == 0:
            self.pos = p1.pos
            self.posSigma = p1.posSigma
        elif cruci[0] == 1:
            posP1 = np.array(p1.pos)
            posP2 = np.array(p2.pos)
            posP1Sigma = np.array(p1.posSigma)
            posP2Sigma = np.array(p2.posSigma)
            self.pos = ((posP1+posP2)/2).astype(int).tolist()
            self.posSigma = ((posP1Sigma + posP2Sigma) / 2).tolist()
        elif cruci[0] == 2:
            a = random.random()
            posP1 = np.array(p1.pos)
            posP2 = np.array(p2.pos)
            posP1Sigma = np.array(p1.posSigma)
            posP2Sigma = np.array(p2.posSigma)
            self.pos = (a*posP1 + (1-a)*posP2).astype(int).tolist()
            self.posSigma = (a*posP1Sigma + (1-a)*posP2Sigma).tolist()
        self.limitsPos(self.xy)

        if cruci[2] == 0:
            self.rgb = p1.rgb
            self.rgbSigma = p1.rgbSigma
        elif cruci[2] == 1:
            rgbP1 = np.array(p1.rgb)
            rgbP2 = np.array(p2.rgb)
            rgbP1Sigma = np.array(p1.rgbSigma)
            rgbP2Sigma = np.array(p2.rgbSigma)
            self.rgb = ((rgbP1 + rgbP2) / 2).astype(int).tolist()
            self.rgbSigma = ((rgbP1Sigma + rgbP2Sigma) / 2).tolist()
        elif cruci[2] == 2:
            a = random.random()
            rgbP1 = np.array(p1.rgb)
            rgbP2 = np.array(p2.rgb)
            rgbP1Sigma = np.array(p1.rgbSigma)
            rgbP2Sigma = np.array(p2.rgbSigma)
            self.rgb = (a * rgbP1 + (1 - a) * rgbP2).astype(int).tolist()
            self.rgbSigma = (a * rgbP1Sigma + (1 - a) * rgbP2Sigma).tolist()
        self.limitsRgb()

    def mutation(self, mutat):
        xi = random.normalvariate(0, 1)
        n = mutat[0]*2 + mutat[1]*2 + mutat[2]*3
        if n!=0:
            tau = 1/math.sqrt(2*math.sqrt(n))
            tauP = 1/math.sqrt(2*n)

        if mutat[1] == 1:
            xiXy = [random.normalvariate(0, 1), random.normalvariate(0, 1)]
            xySigmaX = self.xySigma[0] * math.exp(tauP * xi + tau * xiXy[0])
            xySigmaY = self.xySigma[1] * math.exp(tauP * xi + tau * xiXy[1])
            self.xySigma = [xySigmaX, xySigmaY]
            xyX = self.xy[0] + xySigmaX * random.normalvariate(0, 1)
            xyY = self.xy[1] + xySigmaY * random.normalvariate(0, 1)
            self.xy = [int(xyX), int(xyY)]
        self.limitsXy()

        if mutat[0] == 1:
            xiPos = [random.normalvariate(0, 1), random.normalvariate(0, 1)]
            posSigmaX = self.posSigma[0]*math.exp(tauP*xi + tau*xiPos[0])
            posSigmaY = self.posSigma[1]*math.exp(tauP*xi + tau*xiPos[1])
            self.posSigma = [posSigmaX, posSigmaY]
            posX = self.pos[0]+posSigmaX*random.normalvariate(0, 1)
            posY = self.pos[1]+posSigmaY*random.normalvariate(0, 1)
            self.pos = [int(posX), int(posY)]
        self.limitsPos(self.xy)

        if mutat[2] == 1:
            xiRgb = [random.normalvariate(0, 1), random.normalvariate(0, 1), random.normalvariate(0, 1)]
            rgbSigmaR = self.rgbSigma[0] * math.exp(tauP * xi + tau * xiRgb[0])
            rgbSigmaG = self.rgbSigma[1] * math.exp(tauP * xi + tau * xiRgb[1])
            rgbSigmaB = self.rgbSigma[2] * math.exp(tauP * xi + tau * xiRgb[2])
            self.rgbSigma = [rgbSigmaR, rgbSigmaG, rgbSigmaB]
            rgbR = self.rgb[0]+rgbSigmaR * random.normalvariate(0, 1)
            rgbG = self.rgb[1] + rgbSigmaG * random.normalvariate(0, 1)
            rgbB = self.rgb[2] + rgbSigmaB * random.normalvariate(0, 1)
            self.rgb = [int(rgbR), int(rgbG), int(rgbB)]
        self.limitsRgb()